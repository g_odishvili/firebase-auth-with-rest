package com.android.firebaseauth.entity

data class Authentification(
    val email: String,
    val password: String,
    val returnSecureToken: Boolean? = true,
)