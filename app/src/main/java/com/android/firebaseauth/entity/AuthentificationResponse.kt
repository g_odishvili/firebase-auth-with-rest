package com.android.firebaseauth.entity

data class AuthentificationResponse(
    val idToken: String,
    val email: String,
    val refreshToken: String,
    val expiresIn: String,
    val localId: String,
    val registered: Boolean = true
)