package com.android.firebaseauth.ui.registration

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import com.android.firebaseauth.R
import com.android.firebaseauth.base.BaseFragment
import com.android.firebaseauth.databinding.RegistrationFragmentBinding
import com.android.firebaseauth.entity.Authentification

class RegistrationFragment : BaseFragment<RegistrationFragmentBinding, RegistrationViewModel>(
    RegistrationFragmentBinding::inflate,
    RegistrationViewModel::class.java
) {

    override fun start(inflater: LayoutInflater, container: ViewGroup?) {
        observer()

        binding.registerTv.setOnClickListener {
            register()
        }
    }

    private fun observer() {
        viewModel._data.observe(viewLifecycleOwner, {
            if (!it.registered) {
                findNavController().navigate(
                    R.id.action_registrationFragment_to_successFragment,
                    bundleOf("user" to it.email)
                )
            }
        })
    }

    private fun register() {
        Log.i("register", "inRegister")

        val email = binding.etEmail.text.trim()
        val fullName = binding.etFullName.text.trim()
        val pass = binding.etPassword.text.trim()
        val cnfPass = binding.etConfirmPassword.text.trim()
        val phone = binding.etPhoneNumber.text.trim()
        if (email.isEmpty() || fullName.isEmpty() || cnfPass.isEmpty() || phone.isEmpty()) {
            Toast.makeText(requireContext(), "Please Fill in all the fields", Toast.LENGTH_SHORT)
                .show()
        } else if (pass != cnfPass) {
            Toast.makeText(requireContext(), "Passwords Does not Match", Toast.LENGTH_SHORT).show()
        } else if (pass.length > 5) {
            Toast.makeText(
                requireContext(),
                "Passwords Must be longer than 5 chars",
                Toast.LENGTH_SHORT
            ).show()
        } else {
            val user = Authentification(email as String, pass as String)
            viewModel.register(user)
        }
    }

}