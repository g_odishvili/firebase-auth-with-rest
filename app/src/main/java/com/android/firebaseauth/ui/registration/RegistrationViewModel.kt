package com.android.firebaseauth.ui.registration

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.firebaseauth.api.RetrofitService
import com.android.firebaseauth.entity.Authentification
import com.android.firebaseauth.entity.AuthentificationResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class RegistrationViewModel : ViewModel() {
    private var data = MutableLiveData<AuthentificationResponse>().apply {
    }

    val _data get() = data

    fun register(register: Authentification) {
        viewModelScope.launch {
            withContext(Dispatchers.Default) {
                val response = RetrofitService.RETROFIT.register(register)
                if (response.isSuccessful) {
                    val body = response.body()
                    data.postValue(body)
                }
            }
        }
    }
}