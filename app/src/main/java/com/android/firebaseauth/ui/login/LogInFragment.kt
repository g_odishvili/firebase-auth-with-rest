package com.android.firebaseauth.ui.login

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import com.android.firebaseauth.R
import com.android.firebaseauth.base.BaseFragment
import com.android.firebaseauth.databinding.LogInFragmentBinding
import com.android.firebaseauth.entity.Authentification

class LogInFragment : BaseFragment<LogInFragmentBinding, LogInViewModel>(
    LogInFragmentBinding::inflate,
    LogInViewModel::class.java
) {

    override fun start(inflater: LayoutInflater, container: ViewGroup?) {
        observer()
        initListeners()
    }

    private fun initListeners() {
        binding.loginBtn.setOnClickListener {
            checkInput()
        }

        binding.signupBtn.setOnClickListener {
            findNavController().navigate(R.id.action_logInFragment_to_registrationFragment)
        }
    }

    private fun observer() {
        viewModel._data.observe(viewLifecycleOwner, {
            if (it.registered) {
                findNavController().navigate(
                    R.id.action_logInFragment_to_successFragment,
                    bundleOf("user" to it.email)
                )
            }
        })
    }

    private fun checkInput() {
        val email = binding.emailEdtText.text.trim()
        val pass = binding.passEdtText.text.trim()
        if (email.isEmpty() || pass.isEmpty()) {
            Toast.makeText(requireContext(), "Please Fill in all the fields", Toast.LENGTH_SHORT)
                .show()
        } else {
            val user = Authentification(email as String, pass as String)
            viewModel.login(user)
        }
    }

}