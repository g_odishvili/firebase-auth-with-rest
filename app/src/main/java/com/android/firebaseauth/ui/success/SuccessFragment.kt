package com.android.firebaseauth.ui.success

import android.view.LayoutInflater
import android.view.ViewGroup
import com.android.firebaseauth.base.BaseFragment
import com.android.firebaseauth.databinding.SuccessFragmentBinding

class SuccessFragment : BaseFragment<SuccessFragmentBinding, SuccessViewModel>(
    SuccessFragmentBinding::inflate,
    SuccessViewModel::class.java
) {
    override fun start(inflater: LayoutInflater, container: ViewGroup?) {
        binding.user.text = arguments?.getString("user")
    }

}