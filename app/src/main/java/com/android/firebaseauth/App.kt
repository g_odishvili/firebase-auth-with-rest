package com.android.firebaseauth

import android.app.Application
import java.net.URLEncoder

class App : Application() {
    private external fun firebaseKey(): String

    companion object {
        lateinit var key: String

        // Used to load the 'native-lib' library on application startup.
        init {
            System.loadLibrary("native-lib")
        }
    }

    override fun onCreate() {
        super.onCreate()
        key = URLEncoder.encode(firebaseKey(), "utf-8")
    }
}