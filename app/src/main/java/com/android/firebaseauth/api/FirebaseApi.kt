package com.android.firebaseauth.api


import com.android.firebaseauth.App
import com.android.firebaseauth.entity.Authentification
import com.android.firebaseauth.entity.AuthentificationResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Query


interface FirebaseApi {

    @POST("/v1/accounts:signInWithPassword")
    suspend fun signIn(
        @Body login: Authentification,
        @Query("key") key: String = App.key
    ): Response<AuthentificationResponse>


    @POST("/v1/accounts:signUp")
    suspend fun register(
        @Body register: Authentification,
        @Query("key") key: String = App.key
    ): Response<AuthentificationResponse>
}