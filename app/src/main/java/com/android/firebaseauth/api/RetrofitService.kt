package com.android.firebaseauth.api


import com.android.firebaseauth.CONSTANTS
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitService {

    val RETROFIT: FirebaseApi by lazy {
        Retrofit.Builder()
            .baseUrl(CONSTANTS.FIREBASE_END_POINT)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(FirebaseApi::class.java)
    }

}