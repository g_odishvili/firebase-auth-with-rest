#include <jni.h>
#include <string>

extern "C" JNIEXPORT jstring JNICALL
Java_com_android_firebaseauth_App_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}

extern "C" JNIEXPORT jstring JNICALL
Java_com_android_firebaseauth_App_firebaseKey(
        JNIEnv *env,
        jobject /* this */) {
    std::string key = "AIzaSyC0pVmu7GXpSJt-VTyqu5KenxWKzC4dmUc";
    return env->NewStringUTF(key.c_str());
}